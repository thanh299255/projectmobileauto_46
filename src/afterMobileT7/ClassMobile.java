package afterMobileT7;

public class ClassMobile {

    String nameHS ;
    String nameClass ;
    int slHS ;
    float diemToan ;
    double diemVan ;


    // 1 . Viet cac hàm kiểu dữ liệu  : int ,float ,string ,double
    // Tái sử dụng các hàm kiểu dữ liệu : int ,float ,string ,double


      //1 . Hàm trả về kiểu dữ liệu float

    public float getDiemToanFollowNameHs(String nameHs ) {

        if (nameHs.contentEquals("Doan")) {
            diemToan = 9;

        }
        else
        {
            System.out.print("Not Found Name HS ");
        }
        return  diemToan ;

    }


    //1 . Hàm trả về kiểu dữ liệu int

    public int getSlHSFollowClassName (String nameClass) {
        if(nameClass.equals("9A")) {
            slHS =100;
        }
        else
        {
            System.out.print("Not Found Class: " +nameClass );
        }
        return  slHS;
    }


    //3 . Ham tra ve double
    public double getDiemVanFollowNameHs(String nameHs ) {

        if (nameHs.contentEquals("Doan")) {
            diemVan = 9;

        }
        else
        {
            System.out.print("Not Found Diem Van of HS is :  " + nameHs);
        }
        return  diemVan ;

    }



    // Tai su dung cac ham phia tren : Int  ,Float ,String ,Double
    // Tai su dung cac ham phia tren : Int  ,Float ,String ,Double
    public double getTotalFollowNameHs(String nameHs ) {
        double total ;
        diemToan = getDiemToanFollowNameHs("Doan");
        diemVan = getDiemVanFollowNameHs("Doan");
        total = diemToan+diemVan;
        return  total;
    }






    // 2. 1 class có ít nhất 3 constructor
    ClassMobile() {
        // default
    }

    ClassMobile(String nameClass) {
        this.nameClass = nameClass;
    }

    ClassMobile(String nameClass,String nameHS) {
        this.nameClass = nameClass ;
        this.nameHS = nameHS ;
    }

    ClassMobile(String nameClass,String nameHS,float diemToan) {
        this.nameClass = nameClass ;
        this.nameHS = nameHS ;
        this.diemToan = diemToan ;
    }




    // 3 . Các class xủ dụng lẫn nhau

    //a .call method of class Khoa

    public float getSLHSFollowKhoa(String tenKhoa) {
         Khoa kh = new Khoa() ;
        float diemSo =  kh.getDiemTungKhoa("DHCNT") ;

        return diemSo;


    }


    // call constructor of class Khoa


    Khoa ctr2 = new Khoa("", "",1);









}
