package main.mobileT7;

public class Constructor {
        //fields (or instance variable)
        String webName;
        int webAge;

        // constructor
        Constructor(String name, int age){
            this.webName = name;
            this.webAge = age;
        }


        //1 param
           Constructor(String name){
            this.webName = name;

         }
     //ko có param
        Constructor(){

        }
        // Lưu ý : 1. Tên của constructor phải trùng với tên của class

        //2 . Constructor có thể có nhiều param or ko có param


        //3 . Constructor sâu chuổi lẫn nhau





        public static void main(String args[]){
            //Creating objects
            Constructor obj1 = new Constructor("Automation Mobile", 5);
            Constructor obj2 = new Constructor("Class Auto", 18);

            //ko param
            Constructor object = new Constructor();
            //Accessing object data through reference
            System.out.println(object+"Test");
            System.out.println(obj1.webName+" "+obj1.webAge);
            System.out.println(obj2.webName+" "+obj2.webAge);
        }


}
