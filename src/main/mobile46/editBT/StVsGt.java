package main.mobile46.editBT;

public class StVsGt {

    private String nameCty ;
    private int slNV ;
    private float tlNV ;
    private String bP ;
    private String cV;
    private String tenNV ;

    StVsGt(){
    }


    StVsGt( String nameCty , int slNV , float tlNV, String bP, String cV, String tenNV ) {
        this.nameCty =nameCty ;
        this.slNV=slNV;
        this.tlNV=tlNV;
        this.bP=bP;
        this.cV=cV;
        this.tenNV=tenNV;
    }


    public String getNameCty() {
        return nameCty;
    }

    public void setNameCty(String nameCty) {
        this.nameCty = nameCty;
    }

    public int getSlNV() {
        return slNV;
    }

    public void setSlNV(int slNV) {
        this.slNV = slNV;
    }

    public float getTlNV() {
        return tlNV;
    }

    public void setTlNV(float tlNV) {
        this.tlNV = tlNV;
    }

    public String getbP() {
        return bP;
    }

    public void setbP(String bP) {
        this.bP = bP;
    }

    public String getcV() {
        return cV;
    }

    public void setcV(String cV) {
        this.cV = cV;
    }

    public String getTenNV() {
        return tenNV;
    }

    public void setTenNV(String tenNV) {
        this.tenNV = tenNV;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    private String address ;

}
