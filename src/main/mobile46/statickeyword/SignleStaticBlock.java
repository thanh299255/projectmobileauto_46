package main.mobile46.statickeyword;

public class SignleStaticBlock {


        static int num;
        static String mystr;  // variable keyword static



        static{
            num = 97;
            mystr = "Static keyword in Java";
            if (mystr.isEmpty()) {
                System.out.print("");

            }
        }  // single static block



//        public String getFN() {
//            return mystr;
//        }

        public static void main(String args[])
        {

            System.out.println("Value of num: "+num);
            System.out.println("Value of mystr: "+mystr);
        }


}
