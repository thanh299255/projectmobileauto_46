package main.mobile46.xudungham;

public class ReturnInt {



    // TH1 : default khi khong xu ly code
    //vidu 1
    public static int getSTT() {
        return  0 ;
    }

    // vi du 2
    public static int getSTT(int sl) {
        return sl;
    }

    //vidu3
    public static int getTotal(int a1,int a2) {
        return a2-a1;
    }



    //TH2 : Xu ly code

    //vidu 1 : ko param
    public static int getValueInt() {
        //khai bao bien int
        int total;
        int t1 =10 ;
        int t2 =30 ;

        total = t1 + t2 ;

        return  total;

    }

    //vidu 2 : co params
    public static int getValueInt(String tenNha){
        int valueInt = 0 ;
        if(tenNha.isEmpty()) {
            return  valueInt;
        }
        else {
            return  valueInt +2 ;
        }


    }

    // vi du 3 : mutil param 1
    public static int getTotalAll(int value1 , int  value2) {

        int hSo =0;
        if(value1!=0 && value2>0) {
            hSo = value1 - value2 ;
        }
//            else
//            {
//                hSo = value1 ;
//            }
        return hSo ;
    }




    // Ham xu dung ham cua vidu 3
    public static int taiSuDungHam3(int a,int b ) {
        int tenNha = getTotalAll(a,b);
        if(tenNha!=0) {
            System.out.print("Not found"+tenNha);
        }
        return tenNha;

    }






}
