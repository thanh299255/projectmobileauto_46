package main.mobile46.overloadingmethod;


class DisplayOverloading
{
    public void disp(char c)
    {
        System.out.println(c);
    } //1  ==>  1 method code

    public void disp(int c)
    {
        System.out.println(c);
    } //2 ==> method code



    public void disp(String c)
    {
        System.out.println(c);
    } //3

    public void disp(float c)
    {
        System.out.println(c);
    }//4

    public void disp(double c)
    {
        System.out.println(c);
    } //5

    public void disp(char c, int num)
    {
        System.out.println(c + " "+num);
    } //6

    public void disp(int num,char c)
    {
        System.out.println(c + " "+num);
    } //6




}





//unit test
class Sample


{
    public static void main(String args[])
    {
        DisplayOverloading obj = new DisplayOverloading();
        obj.disp('a');
        obj.disp('a',10);
    }
}
