package main.mobile46.abstactclass.contructor;

public class House {

    String firstName ;
    int b ;
    House() {

    } // object 1

    House(String firstName) {
        this.firstName= firstName;

    } //object 2

    House(String firstName,int b) {
        this.firstName= firstName;
        this.b= b ;

    } //object 3

    House(int b,String firstName) {
        this.firstName= firstName;
        this.b= b ;

    } //object 4


    public static void main(String args[]){
        //Creating objects
        House obj1 = new House("Automation Mobile", 5);
        House obj2 = new House("Class Auto");

        //Accessing object data through reference
        System.out.println(obj1.firstName+" "+obj1.b);

        System.out.println(obj2.firstName);
    }



    // Name sample Class parent
    // Cùng tên ,cùng sô param của constructor thì vẫn được , kiểu dữ liệu theo thứ tự
    // từ trái sang không được giống nhau
    // Nó giống như  Object  ( nhưng phạm vi của nó trong chính constructor đó thôi )


}
