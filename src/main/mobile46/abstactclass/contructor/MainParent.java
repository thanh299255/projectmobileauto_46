package main.mobile46.abstactclass.contructor;

public class MainParent {

    MainParent(){
        System.out.println("MyParentClass Constructor"+"\n");
    } //parent




}
 class MyChildClass extends MainParent{

 //extends : kế thừa toàn bộ những gì class bên trên đang có

    MyChildClass() {
        // đang hiẻu là mình đang dùng kế thừa từ thằng cha
        System.out.println("MyChildClass Constructor 1");
    }  //child  == > super() ;



//    public int getAge() {
//
//
//        return 0;
//    }


    public static void main(String args[]) {
        new MyChildClass();
    }

}
