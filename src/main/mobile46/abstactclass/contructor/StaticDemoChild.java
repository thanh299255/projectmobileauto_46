package main.mobile46.abstactclass.contructor;

public class StaticDemoChild extends StaticDemo {
   public static String a ;

    static {
        System.out.println("static block of child class"+a);
    }

    public void display() {
        System.out.println("Just a method of child class");
    }

    public static void main(String args[]) {
        StaticDemoChild obj = new StaticDemoChild();
        obj.display();
    }

// 1 . constructor static ko đặt tên giống với tên class cha
// 2. nó dùng keyword static là tên constructor
// 3. muốn xử dụng biến phải dùng dạng biến public static
// 4 . Nó ko dùng dc keyword super() ; // nhưng kế thừa dc
// 5. Nó ko tính handle được như constructor thông thường


}