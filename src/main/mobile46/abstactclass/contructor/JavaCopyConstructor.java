package main.mobile46.abstactclass.contructor;

 class JavaCopyConstructor {

        String web;


       JavaCopyConstructor(String w){
             web = w;
        }  //avaCopyConstructor je 1

        /* This is the Copy Constructor, it
         * copies the values of one object
         * to the another object (the object
         * that invokes this constructor)
         */


        JavaCopyConstructor(JavaCopyConstructor je){
            web = je.web;
        }
        //2



        void disp(){
            System.out.println("Website1: "+web);
        }



        public static void main(String args[]){
            JavaCopyConstructor obj1 = new JavaCopyConstructor("Automation Class 1"); // 1

            /* Passing the object as an argument to the constructor
             * This will invoke the copy constructor
             */
            JavaCopyConstructor obj2 = new JavaCopyConstructor(obj1);  // 2 copy data of so 1 lam params

            obj1.disp();
            obj2.disp();
        }
    }

