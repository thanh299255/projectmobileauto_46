package main.mobile46.abstactclass.contructor;

public class main {

    public static void main (String [] args) {
        ClassParent params = new ClassParent();
        int age = params.getAge();
        String firstName = params.getFirstName();
        String lastName = params.getlastName();

        ConstructorSC ob1 = new ConstructorSC(firstName,age,lastName);
        ConstructorSC ob2 = new ConstructorSC(firstName,age);


        System.out.print(ob1.age+"--"+ob1.firstName+"--"+ob1.lastName+"\n");
        System.out.print(ob2.age+"--"+ob2.firstName);




    }
}
