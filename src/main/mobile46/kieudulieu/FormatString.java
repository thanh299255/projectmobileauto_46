package main.mobile46.kieudulieu;

import java.util.ArrayList;
import java.util.List;

public class FormatString {
    public static void main(String[] args) {

        // ví dụ 1


        // so sanh String ( kiểu dữ liệu chuổi )
        // !=null , isEmtry , equals , contentEquals , ...
//        String firstName ="Thanh" ;
//        String lastName  ;

//        String a = "hello world";
//        String b = "nguyen van A";
//        int c =1 ;
//        float d =2;
//
//        if (a.contains("h")) {
//            System.out.print(b);
//        }
//        else
//            System.out.print(a+b+c+d);
//
//    }

//vi du 2


        // vi du 3 : Mảng string
        String a = "hello world";
        String b = "nguyen van A";
        String c = " B";

        List<String> listArray = new ArrayList<>();


        listArray.add(a); //add 1 gía trị vào list String
        listArray.add(b);
        listArray.add(c);

        listArray.remove(a); //xoa a ra khỏi list

        listArray.add(1, a);
        listArray.add(0, c); //add này là add chuổi vào vị trí nào của mảng
        // index của hàm add là số thứ tự của mảng ( mảng thì luôn bắt đầu bằng 0 )

        listArray.add(4, a);


        System.out.print("Gia tri of list is :" + listArray);

        // khi mảng được xác định độ dài mảng lúc ban đầu thì giá trị đưa vào mảng ko được vượt quá độ dài mảng )


    }




}
