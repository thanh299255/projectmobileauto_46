package main.mobile46.aggregation;

// address  : street, phuong/xa,quan/huyen,tinh/tp
// student : tenHs,AgeHS, address (street, phuong/xa,quan/huyen,tinh/tp )




class Address
{
    int streetNum;
    String city;
    String state;
    String country;

    Address(int street, String city, String state, String country)
    {
        this.streetNum=street;
        this.city =city;
        this.state = state;
        this.country = country;
    }
}


class StudentClass
{
    int rollNum;
    String studentName;
    //Creating HAS-A relationship with Address class
    Address studentAddress;



    StudentClass(int roll, String name, Address addr){
        this.rollNum=roll;
        this.studentName=name;
        this.studentAddress = addr;
    }



    public static void main(String args[]){
        // xu ly gia paramter constructor
        Address ad = new Address(55, "Agra", "UP", "India");

       //
        StudentClass obj = new StudentClass(123, "Chaitanya", ad);


        System.out.println(obj.rollNum);
        System.out.println(obj.studentName);
        System.out.println(obj.studentAddress.streetNum);
        System.out.println(obj.studentAddress.city);
        System.out.println(obj.studentAddress.state);
        System.out.println(obj.studentAddress.country);


    }
}
