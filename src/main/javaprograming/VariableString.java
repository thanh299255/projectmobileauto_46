package main.javaprograming;

import sun.jvm.hotspot.utilities.Assert;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class VariableString {

    public static void main(String[] args) {


        // Kiểu dữ liệu java

        /**
         * 1. Kiểu dữ liệu string
         */


        // Biến String rổng ,null ==> vi du 1
//        String firstName = "";
//        String lastName = null;
//
//        if (firstName != null) {
//            System.out.print(firstName + "-----------");
//        } else {
//            System.out.print("not found ");
//        }


        // vi du 2
//        String a = "hello world";
//        String b = "nguyen van A";
//        int c =1 ;
//        float d =2;
//
//        if (a.contentEquals(b)) {
//            System.out.print(b);
//        }
//        else
//            System.out.print(a+b+c+d);
//
//    }

        // contains() , equals() , contentEquals()  ==> so sánh dành cho kiểu dữ liệu string ( object compare )
        // isEmpty() , ! =null  ( để check giá trị null or rỗng của biến Sring )


        // vi du 3 : Mảng string
        String a = "hello world";
        String b = "nguyen van A";
        String c = " B";


        List<String> listArray = new ArrayList<>();

        listArray.add(a); //add 1 gía trị vào list String
        listArray.add(b);
        listArray.add(c);

        listArray.remove(a); //xoa a ra khỏi list

        listArray.add(1, a);
        listArray.add(0, c); //add này là add chuổi vào vị trí nào của mảng
        // index của hàm add là số thứ tự của mảng ( mảng thì luôn bắt đầu bằng 0 )

        listArray.add(5, a);


        System.out.print("Gia tri of list is :" + listArray);

        // khi mảng được xác định độ dài mảng lúc ban đầu thì giá trị đưa vào mảng ko được vượt quá độ dài mảng )


    }


}