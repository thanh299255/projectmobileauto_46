package main.javaprograming.classHandle;

public class TaoHamKieuString {
    // TH1 : default khi khong xu ly code
    //vidu 1
    public static String getFirstName() {
        return  "" ; // or return null ;
    }
    // vi du 2
    public static String getLastName(String lastName) {
        return  lastName; //or   return  ""  // or return null ;
    }

     //vidu3
    public static String getLastName(String lastName,String firstName) {
        return  lastName+firstName; //or   return  ""  // or return null ;
    }

  //TH2 : Xu ly code
    //vidu 1 : ko params
    public static String getOneHouse(){

        String tenNha ="A" ;
        if(tenNha.isEmpty()) {
            return "Nha A" ;
        }
        else {
            return  "khong ton tai" +tenNha ;

        }
    }

    //vidu2 : 1 param
    public static String getOneHouse(String house){

        String address ="" ;
        if(house.contains("A")) {
            address ="so 1 Dang Quoc Dung" ;
        }
        else
        {
            address = "ko tim thay dia chi nha" ;

        }
        return address;


    }


    // vi du 2

    public static String getOneHouse(String address , String tenCN) {

        String tenNha = "";
        if (address.equals("A") && tenCN.equals("D")) {
            tenNha = "Nha Lau A";
        } else {
            tenNha = "Nha Lau B";

        }
        return tenNha;

    }


    //vidu 3
    public String getOneHouseT( String address,String tenCN) {

        String tenNha = "";
        if (address.equals("A") && tenCN.equals("D")) {
            tenNha = "Nha Lau A";
        } else {
            tenNha = "Nha Lau B";

        }
        return tenNha;

    }

    // Ham xu dung ham cua vidu 3
    public String taiSuDungHam3(String address,String tenCN ) {
      String tenNha = getOneHouseT(address,tenCN);
//        String tenNha1= getOneHouseT("ss","sss");
        if(tenNha.isEmpty()) {
            System.out.print("Not found"+tenNha);
        }
        return tenNha;

    }





}
