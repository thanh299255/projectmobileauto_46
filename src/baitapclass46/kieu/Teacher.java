package baitapclass46.kieu;

import javax.lang.model.SourceVersion;

public class Teacher {
    String name;
    double salary;
    int yearEx;


    //    Constructor 1
    Teacher(){
    }

    //    Constructor 2
    Teacher(String name, double salary){
        this.name = name;
        this.salary = salary;
    }  //contructor 2 de xu ly


    //    Constructor 3:
    Teacher(String name, double salary, int year){
        this.name = name;
        this.salary = salary;
        this.yearEx = year;
    }


    //String
    public String getName() {
        return name;
    }


    //opbject
    public void setName(String name) {
        this.name = name;
    }


    //
    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }




    public int getYearEx() {
        return yearEx;
    }

    public void setYearEx(int yearEx) {
        this.yearEx = yearEx;
    }


    //    Kieu du lieu tra ve String
    public String getFullName(String lastName){
        String firstName = name;
        String fullName;

        if(!firstName.isEmpty()) {
            fullName = lastName +" " + firstName;
            return fullName;
        }
        else {
            return firstName;
        }

    }


    //    Kieu du lieu double
    public  double getSalaryTotal(){
        double s = salary;
        double y = yearEx;
        double total = 0;

        if(s >= 4.500 && y >=3){
            total = s * 2.3;
            return total;
        }
        else {
            return s;
        }

    }

    public int getAnnualLeaveNumber(){
        int year = yearEx;

        if(year >= 7){
            return 5;
        }
        if(year >= 3){
            return 2;
        }
        else {
            return 1;
        }

    }


//
//    Student(String name, int age, float score){
//        this.name = name;
//        this.age = age;
//        this.score = score;
//    }

   // Call xử dụng constructor 3 class với nhau
    public String getInfoConstruct2Student() {
        Student ctr2 = new Student("Kieu", 1, 2);

        String valueString = ctr2.name + " "+ctr2.age +  " " + ctr2.score;
        return valueString;

    }
  //Call xử dụng constructor 3 class với nhau
    public String getInfoConstruct2Student(Student ctr2) {
        String valueString = ctr2.name + " "+ctr2.age +  " " + ctr2.score;
        return valueString;

    }




    // Student ( Teacher ,Grade )  chưa xử dụng
    //  chưa sử dụng constructor of clas khác


}