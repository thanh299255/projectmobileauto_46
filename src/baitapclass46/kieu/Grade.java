package baitapclass46.kieu;


public class Grade {
    String teacherName;
    int numberOfStudent;
    String gradeName;


    public Grade() {

    }

    public Grade(String teacherName, String gradeName) {
        this.teacherName = teacherName;
        this.gradeName = gradeName;

    }

    public Grade(String teacherName, int numberOfStudent, String gradeName) {
        this.teacherName = teacherName;
        this.numberOfStudent = numberOfStudent;
        this.gradeName = gradeName;

    }



    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public int getNumberOfStudent() {
        return numberOfStudent;
    }

    public void setNumberOfStudent(int numberOfStudent) {
        this.numberOfStudent = numberOfStudent;
    }

    public String getGradeName() {
        return gradeName;
    }

    public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }



    // string
    public String getTeacherNameAndGradeName(int year) {
        String information = "";
        String yearExperience = "";

        if (numberOfStudent > 0) {
            information = "GV " + getTeacherName() + " day lop " + getGradeName();

            if (year >= 7) {
                yearExperience = " voi tren " + year + " nam kinh nghiem";
            } else {
                yearExperience = " voi duoi " + (year + 1) + " nam kinh nghiem";
            }
        } else {
            information = "GV " + getTeacherName() + " chua co lop ";
        }


        return information + yearExperience;
    }



   // sd contructor of class Teacher

//    Teacher(String name, double salary, int year){
//        this.name = name;
//        this.salary = salary;
//        this.yearEx = year;
//    }

    public int getTotalTN(Teacher ctr3) {
       int TotalTN ;
       String infoTeacherCtr3 = ctr3.name + ctr3.salary + ctr3.yearEx;

       if (!infoTeacherCtr3.isEmpty()) {
           TotalTN = 1000000;
       }
       else
       {
           TotalTN = 999999;
       }

       return  TotalTN ;

    }





    // Student ( Teacher ,Grade )  chưa xử dụng
    //  chưa sử dụng constructor of clas khác
}

