package baitapclass46.kieu;



import java.net.SocketOption;

public class Student {
    String name;
    int age;
    float salary;
    float score;
    float height;

    //    Constructor 1
    Student(){
    }

    //    Constructor 2
    Student(String name, int age, float score){
        this.name = name;
        this.age = age;
        this.score = score;
    }

    //    Constructor 3
    Student(String name, int age, float score, float height){
        this.name = name;
        this.age = age;
        this.score = score;
        this.height = height;
    }

    // String
    public String getName() {
        return name;
    }


    // void
    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }


    public void setAge(int age) {
        this.age = age;
    }

    public float getScore() {
        return score;
    }

    public void setScore(float score) {
        this.score = score;
    }

    //    Kieu tra ve String
    public String getNameAndScore(){
        if (!name.equals("")){
            return ("Ten cua SV la: " + name + ",voi Score: " + score);
        }
        else {
            return "Vui long nhap ten SV, ten SV khong duoc rong";
        }
    }


    // Kieu tra ve Float
    public float getScoreTotal(){

        float total;

        if (score >= 5.0){
            total = score * 1.5f;
            return total;
        }
        else {
            return score;

        }

    }


    // int
    public int getHeightIsCentimeter(){
        return (int) (height *100);

    }



    // Using contructor Teacher
    // Teacher(String name, double salary){

//    Teacher ctr1 = new Teacher("Thanh",10000);

    public String getNameNV(Teacher ctr1) {
//         ctr1 = new Teacher("Thanh",1000); // contructor 2 ,
         String tenNV = ctr1.name+" "+ctr1.salary;
        return tenNV;
    }

    //Các Class (object ) xử dụng các hàm của nhau
    public double getValueDouble(double value) {
        Teacher t = new Teacher();
        value = t.getSalaryTotal();
        return value;
    }






      // Student ( Teacher ,G )  chưa xử dụng
     //  chưa sử dụng constructor of clas khác


}