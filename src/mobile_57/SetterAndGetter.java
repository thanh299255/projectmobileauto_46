package mobile_57;

public class SetterAndGetter {

   // Setter
    // Kiểu String
    String firstName ;
    String lastName;
    String phoneNumber;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
       this.phoneNumber= phoneNumber;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }



    // Getter
    public String getFirstName() {
        return firstName;
    }



    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


}
