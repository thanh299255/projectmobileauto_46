package mobile_57.statickeyword;




class ClassStatic{
    private static String str = "Automation Mobile Class";
        //Static class
        static class MyNestedClass{
        //non-static method
        public void disp() {
            System.out.println(str);

        }
        //static class

//        static class MyNestedClass1{
//            //non-static method
//            public void disp1() {
//                System.out.println(str);
//            }

    }
    public static void main(String args[])
    {
        /* To create instance of nested class we didn't need the outer
         * class instance but for a regular nested class you would need
         * to create an instance of outer class first
         */

        ClassStatic.MyNestedClass t = new ClassStatic.MyNestedClass();
        ClassStatic.MyNestedClass obj = new ClassStatic.MyNestedClass();



        obj.disp();

    }
}
