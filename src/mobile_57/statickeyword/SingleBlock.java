package mobile_57.statickeyword;

class JavaExample{
    static int num;
    static String mystr;
    static String a ;


    static{

        num = 68;
        mystr = "Block1";
        a = "666";
    }

    //Second static block
    static{

        num = 98;
        mystr = "Block2";
    }

    // static single



    public static void main(String args[])
    {
        System.out.println("Value of num: "+num);
        System.out.println("Value of mystr: "+mystr);
        System.out.println("Value of num: "+a);

    }
}
