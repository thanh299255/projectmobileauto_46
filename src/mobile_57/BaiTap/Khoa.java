package mobile_57.BaiTap;

//import static mobile_57.BaiTap.ClassMobile.getDiemToanFollowNameHs;



public class Khoa {
    String nameClass ;
    int slLop ;
    String nameHs ;
    float diemToan ;
    double  diemVan;
    String nameKhoa ;
    String khoa ;


    // Ham tra ve kieu : int

    public int getSLop(String nameKhoa) {
        if(nameKhoa.equals("CNTT")) {
            slLop = 4 ;
        }
        else {
            System.out.print("Not found Class Name is : "+ nameKhoa);
        }
        return  slLop;

    }


    // Ham tra ve kieu : float
    public float getDiemToanFollowNameKhoa(String nameKhoa) {
        if(nameKhoa.equals("CNTT")) {
            diemToan = 900 ;
        }
        else {
            System.out.print("Not found name khoa is : "+ nameKhoa);
        }
        return  diemToan;

    }

    // Ham tra ve kieu : flodiemVanat
    public double getDiemVanFollowNameKhoa(String nameKhoa) {
        if(nameKhoa.equals("CNTT")) {
            diemVan = 800 ;
        }
        else {
            System.out.print("Not found  nameKhoa is : "+ nameHs);
        }
        return  diemVan;

    }

    // Ham tra ve kieu : String
    public String getKhoaFollowClassName(String nameClass) {
        if(nameClass.equals("CNTT_01")) {
            khoa = "Khoa  CNTT" ;
        }
        else {
            System.out.print("Not found className is : "+ nameClass);
        }
        return  khoa;

    }




    //  sử dụng method của class : ClassMobile

    public float getDieOfHs(float diemToan) {
        ClassMobile dt = new ClassMobile();
        diemToan = dt.getDiemToanFollowNameHs("Thanh") ;
        return diemToan;

    }



    // su dung object of nhau (constructor)


//    ClassMobile (String nameClass, String nameHs){
//        this.nameClass = nameClass;
//        this.nameHs =nameHs;
//    }
    public String getNameClass() {
        ClassMobile dt = new ClassMobile("CNTT_1","Thanh");
          String nameClass = dt.nameClass;
        return nameClass;

    }




    // contructor 1
    Khoa (){
    }
    // contructor 2
    Khoa (String nameClass){
        this.nameClass = nameClass;
    }

    // contructor 3
    Khoa (String nameKhoa, String nameClass){
        this.nameKhoa = nameKhoa;
        this.nameClass =nameClass;
    }

    // contructor 4
    Khoa (String nameClass,float diemToan){
        this.diemToan = diemToan;
        this.nameClass =nameClass;
    }


}
