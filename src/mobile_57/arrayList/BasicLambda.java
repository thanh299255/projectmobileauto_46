package mobile_57.arrayList;
import java.util.ArrayList;
import java.util.function.Consumer;

public class BasicLambda {
    //java lambda




    // parameter -> expression : simple
//        public static void main(String[] args) {
//            ArrayList<Integer> numbers = new ArrayList<Integer>();
//            numbers.add(5);
//            numbers.add(9);
//            numbers.add(8);
//            numbers.add(1);
//
////            for(int i=0;i<numbers.size();i++){
////                System.out.println(numbers.get(i));
////            }
//
//
//            numbers.forEach((n) -> getSl(n));
//
//        }
//
//
//        public static int getSl(Integer n) {
//            System.out.println(n);
//            return n;
//        }
//    }


    //(parameter1, parameter2) -> expression   : wrap



//    public static void main(String[] args) {
//        ArrayList<Integer> numbers = new ArrayList<Integer>();
//        numbers.add(5);
//        numbers.add(9);
//        numbers.add(8);
//        numbers.add(1);
//        Consumer<Integer> method =  (n) -> getSl(n);
//
//        numbers.forEach( method );
//    }
//
//        public static int getSl(Integer n) {
//            System.out.println(n);
//            return n;
//        }



    // create method
    interface StringFunction {
        String run(String str);
    }
    
        public static void main(String[] args) {

            StringFunction exclaim = (s) -> s + "!";
            StringFunction ask = (s) -> s + "?";


            printFormatted("Hello", exclaim);
            printFormatted("Hello", ask);
        }
        public static void printFormatted(String str, StringFunction format) {
            String result = format.run(str);
            System.out.println(result);
        }


    }








    // (parameter1, parameter2) -> { code block } : lock



